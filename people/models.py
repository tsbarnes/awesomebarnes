from django.db import models
from django.contrib.auth.models import AbstractUser


class Person(AbstractUser):
    username = models.CharField(max_length=50, unique=True, primary_key=True)
    preferred_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField()

    def __str__(self):
        return f"{self.preferred_name} {self.last_name}"

