from people.models import Person
from django.db import models


class Project(models.Model):
    user = models.ForeignKey(Person, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.name
